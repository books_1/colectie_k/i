# I

## Content

```
./I. Coman:
I. Coman - Am indragit muntii 0.9 '{Natura}.docx

./I. D. Amusin:
I. D. Amusin - Manuscrisele de la Marea Moarta 1.0 '{MistersiStiinta}.docx

./I. D. Musat:
I. D. Musat - Razboiul iobagilor V2 0.99 '{AventuraIstorica}.docx

./I. Delamisa:
I. Delamisa - Zapezile copilariei 1.0 '{Literatura}.docx

./I. Dragan:
I. Dragan - Carte totala masaj 0.9 '{Diverse}.docx

./I. I. Lajecinikov:
I. I. Lajecinikov - Palatul de gheata 1.0 '{Literatura}.docx

./I. Kadare:
I. Kadare - Accidentul 1.0 '{Literatura}.docx

./I. Ludo:
I. Ludo - Paravanul de Aur - V1 Domnul general guverneaza 1.0 '{Teatru}.docx
I. Ludo - Paravanul de Aur - V2 Starea de asediu 1.0 '{Teatru}.docx
I. Ludo - Paravanul de Aur - V3 Regele Palaelibus 3.0 '{Teatru}.docx
I. Ludo - Paravanul de Aur - V4 Salvatorul 3.0 '{Teatru}.docx
I. Ludo - Paravanul de Aur - V5 Ultimul batalion 1.0 '{Teatru}.docx

./I. M. Stefan:
I. M. Stefan - Lumina purpurie 0.99 '{SF}.docx
I. M. Stefan - Misiune speciala V1 1.0 '{ClubulTemerarilor}.docx
I. M. Stefan - Misiune speciala V2 1.0 '{ClubulTemerarilor}.docx
I. M. Stefan - Omul cu o mie de chipuri V1 1.0 '{ClubulTemerarilor}.docx
I. M. Stefan - Omul cu o mie de chipuri V2 1.0 '{ClubulTemerarilor}.docx
I. M. Stefan - P.G.7 lupta contra cronometru 0.99 '{SF}.docx
I. M. Stefan - Ultimul alb 2.0 '{SF}.docx

./I. Voledi & Alexandru Cerbu:
I. Voledi & Alexandru Cerbu - Coroana regelui Burebista 1.0 '{SF}.docx

./Iacob Negruzzi:
Iacob Negruzzi - Cand o vedeam 1.0 '{Versuri}.docx

./Iain M. Banks:
Iain M. Banks - Cultura - V1 Spectrul lui Phlebas 1.0 '{SF}.docx
Iain M. Banks - Cultura - V2 Jucatorul total 1.0 '{SF}.docx
Iain M. Banks - Cultura - V3 Folosirea armelor 1.0 '{SF}.docx

./Iain Pears:
Iain Pears - Afacerea Rafael 1.0 '{Thriller}.docx
Iain Pears - Misterul Tiziano 1.0 '{Thriller}.docx

./Ian Caldwell & Dustin Thomason:
Ian Caldwell & Dustin Thomason - Misterul manuscrisului 3.0 '{AventuraIstorica}.docx

./Iancu Serban Popa:
Iancu Serban Popa - Din trecutul marilor capitani cosmici 0.8 '{SF}.docx

./Iancu Vacarescu:
Iancu Vacarescu - Primavara amorului 1.0 '{Versuri}.docx

./Ian Fleming:
Ian Fleming - Agentul 007 - V1 Caracatita 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V2 Casino Royale 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V3 Pe cine nu lasi sa moara 2.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V4 Moonraker 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V5 Diamante pentru totdeauna 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V6 din Rusia cu dragoste 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V7 Dr. No 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V8 Goldfinger 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V9 Doar pentru ochii tai 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V10 Operatiunea Thunderball 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V11 Spionul care m-a iubit 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V12 In serviciul secret al maiestatii sale 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V13 A doua sansa 1.0 '{ActiuneComando}.docx
Ian Fleming - Agentul 007 - V14 Pistolul de aur 1.0 '{ActiuneComando}.docx

./Ian Kershaw:
Ian Kershaw - Hitler 0.98 '{Razboi}.docx

./Ian Manook:
Ian Manook - Yeruldelgger 0.9 '{Literatura}.docx

./Ian Mcdonald:
Ian Mcdonald - Necroville 1.0 '{SF}.docx
Ian Mcdonald - Sotia djinului 2.0 '{SF}.docx

./Ian McDonald:
Ian McDonald - Inimi, maini, glasuri 2.0 '{SF}.docx
Ian McDonald - Luna - V1 Luna noua 1.0 '{SF}.docx

./Ian Mcewan:
Ian Mcewan - Cainii negri 0.9 '{Literatura}.docx
Ian Mcewan - Durabila iubire 1.0 '{Literatura}.docx
Ian Mcewan - Gradina de ciment 1.0 '{Literatura}.docx
Ian Mcewan - Inocentul 0.9 '{Literatura}.docx
Ian Mcewan - Mangaieri straine 1.0 '{Literatura}.docx
Ian Mcewan - Pe plaja Chesil 1.0 '{Literatura}.docx
Ian Mcewan - Sambata 0.99 '{Literatura}.docx

./Ian McGuire:
Ian McGuire - Apele nordului 1.0 '{Literatura}.docx

./Iannis Maris:
Iannis Maris - Moartea lui Timoteos Konstas 0.9 '{Politista}.docx

./Ian Rankin:
Ian Rankin - Apelul mortilor 1.0 '{Politista}.docx
Ian Rankin - Rebus - V1 X si zero 1.0 '{Politista}.docx
Ian Rankin - Rebus - V2 De-a v-ati ascunselea 1.0 '{Politista}.docx

./Ian Stewart:
Ian Stewart - Numerele naturii 0.9 '{Matematica}.docx

./Ian Watson:
Ian Watson - Ambasada extraterestra 1.0 '{SF}.docx
Ian Watson - O foarte lenta masina a timpului 1.0 '{SF}.docx
Ian Watson - Pasari lente 0.9 '{SF}.docx
Ian Watson - Spatiul Mana. Recolta norocoasei 1.0 '{SF}.docx

./Ida Simons:
Ida Simons - O fecioara nesabuita 1.0 '{Literatura}.docx

./Idris Seabright:
Idris Seabright - Moartea noastra cea de toate zilele 1.0 '{SF}.docx

./Ignacio Cardenas Acuna:
Ignacio Cardenas Acuna - Enigma unei duminici 1.0 '{Politista}.docx

./Ignacio Garcia Valino:
Ignacio Garcia Valino - Cele doua morti ale lui Socrate 0.99 '{Literatura}.docx

./Ignacio Manuel Altimirano:
Ignacio Manuel Altimirano - El zarco 1.0 '{Western}.docx

./Ignatie Briancianinov:
Ignatie Briancianinov - Despre smerenie 0.7 '{Religie}.docx
Ignatie Briancianinov - Duhul rugaciunii incepatorului 0.7 '{Religie}.docx

./Igor Bauersima:
Igor Bauersima - Norway today 0.9 '{Teatru}.docx

./Igor Bergler:
Igor Bergler - Biblia pierduta 1.0 '{Politista}.docx
Igor Bergler - Testamentul lui Abraham 1.0 '{Politista}.docx

./Igor Bogdanoff:
Igor Bogdanoff - Masina fantoma 1.0 '{SF}.docx

./Igor Botan:
Igor Botan - Armonizarea relatiilor interetnice 0.8 '{Politica}.docx

./Igor Rosohovatski:
Igor Rosohovatski - Noaptea planctonului 1.0 '{Aventura}.docx

./Ihsan Oktay Anar:
Ihsan Oktay Anar - Atlasul continentelor incetosate 1.0 '{Literatura}.docx

./Iisus:
Iisus - Ida Kling - Revelatii noi 0.7 '{Spiritualitate}.docx
Iisus - James Twyman - Arta facatorilor de pace. Curs de iluminare 1.0 '{Spiritualitate}.docx
Iisus - James Twyman - Curs de iluminare. Meditatii 1.0 '{Spiritualitate}.docx
Iisus - Pamela - A treia cale 0.99 '{Spiritualitate}.docx

./Ildefonso Falcones:
Ildefonso Falcones - Catedrala marii 2.0 '{AventuraIstorica}.docx
Ildefonso Falcones - Mana Fatimei 1.0 '{AventuraIstorica}.docx
Ildefonso Falcones - Regina desculta 0.99 '{AventuraIstorica}.docx

./Ildico Achimescu:
Ildico Achimescu - E atat de frumos 0.9 '{Diverse}.docx

./Ileana Vulpescu:
Ileana Vulpescu - Arta compromisului 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Arta conversatiei 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Candidatii la fericire 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Carnetul din port-hart 1.0 '{ClasicRo}.docx
Ileana Vulpescu - De-amor, de-amar, de inima albastra 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Noi, doamna doctor, cand o sa murim 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Nota informativa batuta la masina 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Pe apa sambetei 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Preludiu 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Ramas bun casei parintesti 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Saruta pamantul acesta 1.0 '{ClasicRo}.docx
Ileana Vulpescu - Viata legata cu ata 1.0 '{ClasicRo}.docx

./Ilia Djerekarov:
Ilia Djerekarov - Intalnire neasteptata 0.99 '{SF}.docx

./Ilia Ilf & Evgheni Petrov:
Ilia Ilf & Evgheni Petrov - Atidudine dispretuitoare fata de stomac 1.0 '{Umor}.docx
Ilia Ilf & Evgheni Petrov - Douasprezece scaune 2.0 '{Umor}.docx
Ilia Ilf & Evgheni Petrov - Vitelul de aur 0.99 '{Umor}.docx

./Ilia Varsavski:
Ilia Varsavski - Subiect de roman 1.0 '{Literatura}.docx

./Ilie Cleopa:
Ilie Cleopa - Calauza in credinta ortodoxa 1.0 '{Religie}.docx
Ilie Cleopa - Despre vise si vedenii 1.0 '{Religie}.docx
Ilie Cleopa - Ne vorbeste parintele Cleopa V1-10 1.0 '{Religie}.docx
Ilie Cleopa - Ne vorbeste parintele Cleopa V11-19 1.0 '{Religie}.docx

./Ilie Gherghel:
Ilie Gherghel - Prin campii si plaiuri strabune 0.9 '{Natura}.docx

./Ilie Salceanu:
Ilie Salceanu - Umbra slugerului Theodor 1.0 '{IstoricaRo}.docx

./Ilinca Bernea:
Ilinca Bernea - Ateism, agnosticism si logica lui versus 0.9 '{Filozofie}.docx
Ilinca Bernea - Poeme in mi bemol major 0.99 '{Versuri}.docx

./Ilinca Bernea & Matei Ghigiu & Bogdan Geana:
Ilinca Bernea & Matei Ghigiu & Bogdan Geana - Ignorantii 0.9 '{Versuri}.docx

./Immanuel Wallerstein:
Immanuel Wallerstein - Are capitalismul un viitor 1.0 '{Istorie}.docx

./Indro Montanelli:
Indro Montanelli - Istoria grecilor 1.0 '{Istorie}.docx

./Ines Nollier:
Ines Nollier - Marele maestru al tempiierilor 1.0 '{AventuraIstorica}.docx

./Ingmar Bergman:
Ingmar Bergman - Lanterna magica 1.0 '{Literatura}.docx

./Ioachim Parvulescu:
Ioachim Parvulescu - Sfanta taina a spovedaniei pe intelesul tuturor 1.0 '{Religie}.docx

./Ioana Albu:
Ioana Albu - Dincolo de umbra 2.0 '{SF}.docx

./Ioana Baetica Morpurgo:
Ioana Baetica Morpurgo - Fisa de inregistrare 1.0 '{Erotic}.docx

./Ioana C. Visan:
Ioana C. Visan - Cea mai bogata femeie din lume 0.99 '{SF}.docx
Ioana C. Visan - Din lipsa de timp 0.99 '{SF}.docx
Ioana C. Visan - La distanta de un lift 0.99 '{SF}.docx
Ioana C. Visan - Ne intalnim la capatul drumului 2.0 '{SF}.docx

./Ioana Dimitrescu:
Ioana Dimitrescu - Un strigat in noapte 1.0 '{Politista}.docx

./Ioana Em. Petrescu:
Ioana Em. Petrescu - Modernism - postmodernism 1.0 '{Diverse}.docx

./Ioana Ieronim:
Ioana Ieronim - Poeme electronice 0.9 '{Versuri}.docx

./Ioan Alexandru Bratescu Voinesti:
Ioan Alexandru Bratescu Voinesti - Intuneric si lumina 0.99 '{Diverse}.docx

./Ioana Parvulescu:
Ioana Parvulescu - Viata incepe de vineri 0.8 '{Diverse}.docx
Ioana Parvulescu - Viitorul incepe luni 0.99 '{Diverse}.docx

./Ioana Petrescu:
Ioana Petrescu - Balaurul marilor 0.99 '{SF}.docx

./Ioan Aurel Pop:
Ioan Aurel Pop - Istoria ilustrata a Romaniei 1.0 '{Istorie}.docx

./Ioan Chirila:
Ioan Chirila - Ar fi fost prea frumos! 0.8 '{Sport}.docx
Ioan Chirila - Jurnal sentimental Mexico 1970 0.99 '{Sport}.docx
Ioan Chirila - Lucescu 0.6 '{Sport}.docx
Ioan Chirila - Portile cerului 0.7 '{Sport}.docx

./Ioan Dan:
Ioan Dan - Cavalerii - V1 Cavalerii ordinului Basarab 5.0 '{AventuraIstorica}.docx
Ioan Dan - Cavalerii - V2 Curierul secret 5.0 '{AventuraIstorica}.docx
Ioan Dan - Cavalerii - V3 Cavalerii 5.0 '{AventuraIstorica}.docx
Ioan Dan - Cavalerii - V4 Taina cavalerilor 5.0 '{AventuraIstorica}.docx
Ioan Dan - Procesul maresalului Ion Antonescu 1.0 '{IstoricaRo}.docx

./Ioan Dimoftache Musat:
Ioan Dimoftache Musat - Misiunea H. S. 1.0 '{AventuraIstorica}.docx
Ioan Dimoftache Musat - Romanul doamnei Sastok 1.0 '{Literatura}.docx

./Ioan Dolean:
Ioan Dolean - Meseria de parinte 0.9 '{DezvoltarePersonala}.docx

./Ioan Dumitru Denciu:
Ioan Dumitru Denciu - Identitatile lui Litovoi 1.0 '{IstoricaRo}.docx

./Ioan Gansca:
Ioan Gansca - Parintele Arsenie Boca, mare indrumator de suflete din sec. XX 1.0 '{Religie}.docx

./Ioan Grosan:
Ioan Grosan - Epopeea spatiala 2084 0.99 '{SF}.docx
Ioan Grosan - O suta de ani de zile la portile orientului 1.0 '{IstoricaRo}.docx
Ioan Grosan - Planeta mediocrilor 1.0 '{SF}.docx
Ioan Grosan - Un om din est 0.6 '{SF}.docx

./Ioan Hategan:
Ioan Hategan - Filippo Scolari 0.8 '{IstoricaRo}.docx

./Ioan Iancu:
Ioan Iancu - Actiunea Banana 1.0 '{Politista}.docx
Ioan Iancu - Procurorul face recurs 1.0 '{Politista}.docx

./Ioan Ianolide:
Ioan Ianolide - Intoarcerea la Hristos 0.9 '{Politica}.docx

./Ioanichie Balan:
Ioanichie Balan - Nicodim Mandita 1.0 '{Religie}.docx

./Ioan Lacusta:
Ioan Lacusta - Dupa vanzare 0.7 '{Politica}.docx

./Ioan M. Bujoreanu:
Ioan M. Bujoreanu - Mistere din Bucuresti V1 0.8 '{Diverse}.docx
Ioan M. Bujoreanu - Mistere din Bucuresti V2 0.8 '{Diverse}.docx

./Ioan Mandrisor Capusneac:
Ioan Mandrisor Capusneac - Despre invatare 0.8 '{DezvoltarePersonala}.docx

./Ioan Matei:
Ioan Matei - Deportat in tara straina 1.0 '{Memorii}.docx

./Ioan Maximovici de San Francisco:
Ioan Maximovici de San Francisco - Declinul patriarhiei Constantinopolului 0.9 '{Religie}.docx

./Ioan Micu:
Ioan Micu - Enigma pesterii din Tampa V1 1.0 '{ClubulTemerarilor}.docx
Ioan Micu - Enigma pesterii din Tampa V2 1.0 '{ClubulTemerarilor}.docx

./Ioan Muntean:
Ioan Muntean - Elemente de alchimie spirituala 0.9 '{Spiritualitate}.docx
Ioan Muntean - La pas prin reeducarile de la Pitesti Gherla si Aiud 0.6 '{Comunism}.docx

./Ioan Neacsu:
Ioan Neacsu - Introducere in poezie 0.99 '{Diverse}.docx

./Ioan Pavel Petras:
Ioan Pavel Petras - Cartea vietii 0.9 '{Diverse}.docx

./Ioan Petru Culianu:
Ioan Petru Culianu - Arta fugii 1.0 '{Literatura}.docx
Ioan Petru Culianu - Dialoguri intrerupte. Corespondenta cu Mircea Eliade 1.0 '{Literatura}.docx
Ioan Petru Culianu - Hesperus 1.0 '{SF}.docx
Ioan Petru Culianu - Jocul de smarald 1.0 '{Literatura}.docx
Ioan Petru Culianu - Pacatul impotriva spiritului 1.0 '{Literatura}.docx
Ioan Petru Culianu - Pergamentul diafan. Ultimele povestiri 1.0 '{Literatura}.docx

./Ioan Popa:
Ioan Popa - Robi pe Uranus 2.0 '{IstoricaRo}.docx

./Ioan Povara:
Ioan Povara - Speologie 0.9 '{Diverse}.docx

./Ioan Santea:
Ioan Santea - Nopti sangerande 1.0 '{Literatura}.docx
Ioan Santea - Viata ca o descoperire 0.9 '{Literatura}.docx

./Ioan Slavici:
Ioan Slavici - Gura satului si alte nuvele 1.0 '{ClasicRo}.docx
Ioan Slavici - Mara 1.0 '{ClasicRo}.docx
Ioan Slavici - Moara cu noroc 1.0 '{ClasicRo}.docx
Ioan Slavici - Nuvele 1.0 '{ClasicRo}.docx
Ioan Slavici - O viata pierduta 1.0 '{ClasicRo}.docx
Ioan Slavici - Padureanca 1.0 '{ClasicRo}.docx
Ioan Slavici - Popa Tanda 1.0 '{ClasicRo}.docx
Ioan Slavici - Povesti 1.0 '{BasmesiPovesti}.docx
Ioan Slavici - Rodul tainic 1.0 '{ClasicRo}.docx

./Ion Agarbiceanu:
Ion Agarbiceanu - Domnisoara Ana 0.99 '{ClasicRo}.docx
Ion Agarbiceanu - Fefeleaga 0.9 '{ClasicRo}.docx
Ion Agarbiceanu - Opere - V1 Schite si povestiri 1.0 '{ClasicRo}.docx
Ion Agarbiceanu - Opere - V2 Schite si povestiri 0.7 '{ClasicRo}.docx
Ion Agarbiceanu - Opere - V16 0.7 '{ClasicRo}.docx
Ion Agarbiceanu - Pustnicul si ucenicul 0.9 '{ClasicRo}.docx

./Ion Antonescu:
Ion Antonescu - Scrisoarea maresalului Ion Antonescu 0.9 '{Interzise}.docx

./Ion Arama:
Ion Arama - Alarma la dana zero 0.9 '{SF}.docx
Ion Arama - Furtuna alba 0.8 '{SF}.docx
Ion Arama - Luntrasii lui Vlad Voda 2.0 '{IstoricaRo}.docx
Ion Arama - Stanca sperantei 0.6 '{SF}.docx
Ion Arama - Tronsonul B.N. 1.0 '{SF}.docx
Ion Arama - Vedeta 70 ataca 1.0 '{ClubulTemerarilor}.docx

./Ionatan Pirosca:
Ionatan Pirosca - Cu fata la cruce 0.9 '{Versuri}.docx
Ionatan Pirosca - Poema iubirii 0.9 '{Versuri}.docx
Ionatan Pirosca - Tabla inmultirii cu cerul 0.99 '{Versuri}.docx

./Ion Baiesu:
Ion Baiesu - Acceleratorul 1.0 '{Teatru}.docx
Ion Baiesu - Alibi 1.0 '{Teatru}.docx
Ion Baiesu - Ariciul de la dopul perfect 1.0 '{Teatru}.docx
Ion Baiesu - Autorul e in sala 1.0 '{Teatru}.docx
Ion Baiesu - Chitimia 1.0 '{Teatru}.docx
Ion Baiesu - Cine sapa groapa altuia 1.0 '{Teatru}.docx
Ion Baiesu - Experimentul 1.0 '{Teatru}.docx
Ion Baiesu - Fantomiada 1.0 '{Teatru}.docx
Ion Baiesu - Gargarita 1.0 '{Teatru}.docx
Ion Baiesu - Iertarea 1.0 '{Teatru}.docx
Ion Baiesu - Maestrul 1.0 '{Teatru}.docx
Ion Baiesu - Mama s-a indragostit 1.0 '{Teatru}.docx
Ion Baiesu - Puterea dragostei 1.0 '{Teatru}.docx
Ion Baiesu - Suferinta in doi 1.0 '{Teatru}.docx
Ion Baiesu - Sursa 1.0 '{Teatru}.docx
Ion Baiesu - Umor la domiciliu 1.0 '{Teatru}.docx
Ion Baiesu - Vanatorii 1.0 '{Teatru}.docx
Ion Baiesu - Vinovatul 1.0 '{Teatru}.docx

./Ion Barbu:
Ion Barbu - Addenda 0.99 '{Versuri}.docx
Ion Barbu - Din periodice 0.99 '{Versuri}.docx
Ion Barbu - Dupa melci 0.99 '{Versuri}.docx
Ion Barbu - Isarlak 0.99 '{Versuri}.docx
Ion Barbu - Joc secund 0.8 '{Versuri}.docx
Ion Barbu - Uvedenrode 0.99 '{Versuri}.docx

./Ion Blajan:
Ion Blajan - Omul cu ochelari negri 0.9 '{Diverse}.docx

./Ion Bodunescu:
Ion Bodunescu - Inspectorul sef 0.8 '{Politista}.docx

./Ion Brad:
Ion Brad - Descoperirea familiei 1.0 '{Literatura}.docx
Ion Brad - Romanul de familie V1 1.0 '{Literatura}.docx
Ion Brad - Romanul de familie V2 1.0 '{Literatura}.docx

./Ion Budai Deleanu:
Ion Budai Deleanu - Tiganiada 1.0 '{Versuri}.docx

./Ion Calovia:
Ion Calovia - Apokolokyntosis 1.0 '{ClubulTemerarilor}.docx

./Ion Calugaru:
Ion Calugaru - Copilaria unui netrebnic 0.7 '{Diverse}.docx

./Ion Constantin & Anton Bacalbasa:
Ion Constantin & Anton Bacalbasa - Pardon 1.0 '{Teatru}.docx

./Ion Creanga:
Ion Creanga - Amintiri din copilarie. Povesti, povestiri, nuvele 1.0 '{ClasicRo}.docx
Ion Creanga - Amintiri din copilarie. Povestiri 1.0 '{ClasicRo}.docx
Ion Creanga - Articole 0.99 '{ClasicRo}.docx
Ion Creanga - Capra cu trei iezi 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Corespondenta - V1 Catre familie 0.9 '{ClasicRo}.docx
Ion Creanga - Corespondenta - V2 Catre personalitati 0.9 '{ClasicRo}.docx
Ion Creanga - Danila Prepeleac 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Fata babei si fata mosneagului 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Ivan Turbinca 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Mos Ion Roata si unirea 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Oltenii la Iasi 0.99 '{BasmesiPovesti}.docx
Ion Creanga - Postume 0.8 '{ClasicRo}.docx
Ion Creanga - Povestea lui Harap Alb 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Povestea lui Ionica cel Prost 0.99 '{Necenzurat}.docx
Ion Creanga - Povestea porcului 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Povestea povestilor 0.99 '{Necenzurat}.docx
Ion Creanga - Povestea unui om lenes 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Povesti. Povestiri 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Povesti didactice 0.9 '{BasmesiPovesti}.docx
Ion Creanga - Prostia omeneasca 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Punguta cu doi bani 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Soacra cu trei nurori 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Stan Patitul 1.0 '{BasmesiPovesti}.docx
Ion Creanga - Versuri 0.99 '{Versuri}.docx

./Ion Cristoiu:
Ion Cristoiu - Veselia generala 0.7 '{Jurnalism}.docx

./Ion Cucu:
Ion Cucu - Cum ar arata viata fara fotografie 1.0 '{Diverse}.docx

./Ionel Pop:
Ionel Pop - Instantanee din viata animalelor 0.99 '{Natura}.docx

./Ionel Teodoreanu:
Ionel Teodoreanu - Bal mascat 1.0 '{ClasicRo}.docx
Ionel Teodoreanu - Bunicii 0.99 '{ClasicRo}.docx
Ionel Teodoreanu - In casa bunicilor 0.9 '{ClasicRo}.docx
Ionel Teodoreanu - La Medeleni - V1 Hotarul nestatornic 2.0 '{ClasicRo}.docx
Ionel Teodoreanu - La Medeleni - V2 Drumuri V1 2.0 '{ClasicRo}.docx
Ionel Teodoreanu - La Medeleni - V2 Drumuri V2 2.0 '{ClasicRo}.docx
Ionel Teodoreanu - La Medeleni - V3 Intre vanturi 2.0 '{ClasicRo}.docx
Ionel Teodoreanu - Lorelei 2.0 '{ClasicRo}.docx
Ionel Teodoreanu - Sa vie bazarca! 1.0 '{ClasicRo}.docx
Ionel Teodoreanu - Secretul Anei Florentin 1.0 '{ClasicRo}.docx

./Ion Gavrila Ogoranu & Lucia Baki:
Ion Gavrila Ogoranu & Lucia Baki - Brazii se frang, dar nu se indoiesc V3 0.9 '{Politica}.docx

./Ion Ghica:
Ion Ghica - Scrisori catre Vasile Alecsandri 1.0 '{Diverse}.docx

./Ion Gramada:
Ion Gramada - Trei nuvele 0.99 '{Nuvele}.docx

./Ion Grecea:
Ion Grecea - Intr-o singura ora 0.9 '{ActiuneRazboi}.docx
Ion Grecea - La portile Severinului 0.9 '{ActiuneRazboi}.docx

./Ion Heliade Radulescu:
Ion Heliade Radulescu - Fata lui Chiriac 1.0 '{ClasicRo}.docx
Ion Heliade Radulescu - Poezie si proza 3.0 '{ClasicRo}.docx
Ion Heliade Radulescu - Zburatorul 1.0 '{ClasicRo}.docx

./Ion Hobana:
Ion Hobana - Calatorie intrerupta 0.9 '{Literatura}.docx
Ion Hobana - Caleidoscop 0.9 '{Versuri}.docx
Ion Hobana - Emisiune nocturna 0.9 '{SF}.docx
Ion Hobana - Imaginile posibilului 0.7 '{MistersiStiinta}.docx
Ion Hobana - OZN-urile si apararea 0.8 '{MistersiStiinta}.docx
Ion Hobana - Timp pentru dragoste 1.0 '{SF}.docx
Ion Hobana - Ultimul val 0.99 '{Suspans}.docx

./Ion Ioanid:
Ion Ioanid - Inchisoarea noastra cea de toate zilele V1 0.5 '{Literatura}.docx
Ion Ioanid - Inchisoarea noastra cea de toate zilele V2 0.7 '{Literatura}.docx
Ion Ioanid - Inchisoarea noastra cea de toate zilele V3 0.7 '{Literatura}.docx
Ion Ioanid - Inchisoarea noastra cea de toate zilele V5 0.7 '{Literatura}.docx

./Ion Ionescu Bucovu:
Ion Ionescu Bucovu - Fiasco 0.9 '{Diverse}.docx
Ion Ionescu Bucovu - Izvorul fericirii 0.7 '{Diverse}.docx
Ion Ionescu Bucovu - Priveghiul 0.7 '{Diverse}.docx

./Ion Izvor:
Ion Izvor - Cararea din unde 0.99 '{Diverse}.docx

./Ion Luca Caragiale:
Ion Luca Caragiale - 1 aprilie 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Art. 214 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Boborul 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Boris Sarafoff 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Bubico 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Caldura mare 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Condeescu 0.8 '{ClasicRo}.docx
Ion Luca Caragiale - Conu Leonida fata cu reactiunea 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Corespondenta - Scrisori catre Alexandru Vlahuta 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Culisele chestiunii nationale 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Cum stam 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - D'ale carnavalului 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Dl. Goe 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Duminica Tomii 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Hatmanul Baltag 0.8 '{ClasicRo}.docx
Ion Luca Caragiale - Incepem 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Infamie 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - In vreme de razboi 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Istoria se repeta 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Jertfe patriotice 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Justitia romana 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Justitie 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - La hanul lui Manjoala si alte nuvele 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Lantul slabiciunilor 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - La posta 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Moftangii 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Momente si schite. Nuvele si povestiri 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Monopol 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Mosii 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Napasta 0.8 '{ClasicRo}.docx
Ion Luca Caragiale - Natiunea romana 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Nuvele fantastice 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Nuvele si schite 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - O faclie de Paste 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - O intampinare personala 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - O noapte furtunoasa 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Opere alese V1 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Opere alese V2 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - O scrisoare pierduta 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - O soacra 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - O zi solemna 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Pe oceanul vremii 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Poeme si proza 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Politica 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Politica inalta 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Postume 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Proces verbal 0.7 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica. Articole politice 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica. Literatura si cultura 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica V1 0.8 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica V2 0.7 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica V3 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Publicistica V4 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Repaosul dominical 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Romanii verzi 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Schite - Din periodice 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Schite V1 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Schite V2 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Situatiunea 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Teatru 1.0 '{ClasicRo}.docx
Ion Luca Caragiale - Telegrame 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Triumful talentului 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Ultima ora 0.9 '{ClasicRo}.docx
Ion Luca Caragiale - Un pedagog de scoala noua 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Urgent 0.99 '{ClasicRo}.docx
Ion Luca Caragiale - Versuri 0.9 '{ClasicRo}.docx

./Ion Manolescu:
Ion Manolescu - Derapaj 0.99 '{Diverse}.docx

./Ion Manzatu:
Ion Manzatu - Chemarea nesfarsitului 1.0 '{SF}.docx
Ion Manzatu - Ereziile de la Iasi 0.9 '{Diverse}.docx
Ion Manzatu - Paradoxala aventura 1.5 '{SF}.docx

./Ion Manzatu & Gh. Sasarman:
Ion Manzatu & Gh. Sasarman - Catastrofa mezonica 1.0 '{SF}.docx

./Ion Marin Almajan:
Ion Marin Almajan - Vremea hahalerelor 0.8 '{Politica}.docx

./Ion Marin Sadoveanu:
Ion Marin Sadoveanu - Taurul marii 0.9 '{IstoricaRo}.docx

./Ion Minulescu:
Ion Minulescu - Corigent la limba romana 1.0 '{ClasicRo}.docx
Ion Minulescu - De vorba cu mine insumi 1.0 '{Versuri}.docx
Ion Minulescu - Nu sunt ce par a fi 1.0 '{Versuri}.docx
Ion Minulescu - Rosu galben si albastru 1.0 '{ClasicRo}.docx

./Ion Mitru:
Ion Mitru - Diminetile ostasilor 0.6 '{Diverse}.docx

./Ion Neculce:
Ion Neculce - Letopisetul Tarii Moldovei 1.0 '{ClasicRo}.docx

./Ion Nicolae Bucur:
Ion Nicolae Bucur - V1 Sarmis 1.0 '{IstoricaRo}.docx
Ion Nicolae Bucur - V2 Dicomes 1.0 '{IstoricaRo}.docx
Ion Nicolae Bucur - V3 Sargetius 1.0 '{IstoricaRo}.docx
Ion Nicolae Bucur - V4 Comosicus 1.0 '{IstoricaRo}.docx

./Ion Ochinciuc:
Ion Ochinciuc - Egreta violeta 1.0 '{Politista}.docx

./Ion Ochiniciuc:
Ion Ochiniciuc - Spada de Toledo 2.0 '{IstoricaRo}.docx

./Ion Pantazi:
Ion Pantazi - Am trecut prin iad 1.0 '{Razboi}.docx

./Ion Pop Reteganul:
Ion Pop Reteganul - Povesti ardelenesti 1.0 '{BasmesiPovesti}.docx

./Ion Pribeagu:
Ion Pribeagu - Impertinentele 0.99 '{Versuri}.docx
Ion Pribeagu - Mic si al dracului 1.0 '{Umor}.docx
Ion Pribeagu - Poezii 0.99 '{Versuri}.docx

./Ion Ratiu:
Ion Ratiu - Moscova sfideaza lumea 1.0 '{Politica}.docx

./Ion Reteganul:
Ion Reteganul - Povesti ardelenesti 1.0 '{BasmesiPovesti}.docx

./Ion Tarlea:
Ion Tarlea - Un politist isi rupe catusele 0.7 '{Politista}.docx

./Ion Tipsie:
Ion Tipsie - In drum spre oras 2.0 '{Politista}.docx
Ion Tipsie - Orasul din campie 1.0 '{Politista}.docx

./Ion Topolog & Paul Antim:
Ion Topolog & Paul Antim - Lovituri din umbra V1 1.0 '{ClubulTemerarilor}.docx
Ion Topolog & Paul Antim - Lovituri din umbra V2 1.0 '{ClubulTemerarilor}.docx

./Ion Tudor:
Ion Tudor - Misterele timpului - O teorie a transcendentei 0.7 '{MistersiStiinta}.docx

./Ion Tugui:
Ion Tugui - Fenomene paranormale 1.0 '{Spiritualitate}.docx

./Ionut Chiva:
Ionut Chiva - 69 1.0 '{Diverse}.docx

./Ionut Cristache:
Ionut Cristache - Calendarul cu patimi 0.9 '{Diverse}.docx

./Ion Vitner:
Ion Vitner - Albert Camus sau tragicul exilului 0.9 '{Filozofie}.docx

./Ion Vlasiu:
Ion Vlasiu - Am plecat din sat 1.0 '{Diverse}.docx
Ion Vlasiu - O singura iubire 1.0 '{Dragoste}.docx

./Iordan Chimet:
Iordan Chimet - Dosar Mihai Sebastian 0.7 '{Diverse}.docx

./Iosif D. Agapitos:
Iosif D. Agapitos - Sfantul Nectarie, sfantul iubirii 0.9 '{Religie}.docx

./Iosif Micu:
Iosif Micu - Am supravietuit lagarului hitlerist 1.0 '{Razboi}.docx
Iosif Micu - Lagarul mortii, dar si al demnitatii 1.0 '{Razboi}.docx

./Iosif Naghiu:
Iosif Naghiu - Fereastra. Gluga pe ochi 0.9 '{Teatru}.docx
Iosif Naghiu - Intr-o singura seara 1.0 '{Teatru}.docx
Iosif Naghiu - Sub palarie 1.0 '{Teatru}.docx

./Iosub Florin:
Iosub Florin - Problema raului 0.9 '{Religie}.docx

./Ips Pimen:
Ips Pimen - Selectie proloage 0.7 '{Religie}.docx

./Ira Levin:
Ira Levin - Sarutul dinaintea mortii 0.7 '{Diverse}.docx

./Irina Binder:
Irina Binder - Fluturi V1 1.0 '{Dragoste}.docx
Irina Binder - Fluturi V2 1.0 '{Dragoste}.docx

./Irina Fieroiu:
Irina Fieroiu - Claudia 0.9 '{Dragoste}.docx

./Irina Petras:
Irina Petras - Cartile deceniului 10 1.0 '{Diverse}.docx
Irina Petras - Feminitatea limbii romane 0.9 '{Diverse}.docx

./Irina Rita:
Irina Rita - Orele planetare 0.8 '{Astrologie}.docx

./Irina Schulter:
Irina Schulter - Revedere la Ibiza 0.99 '{Dragoste}.docx
Irina Schulter - Tanara si nestiutoare 0.99 '{Dragoste}.docx

./Iris Andersen:
Iris Andersen - Chipul trecutului 1.0 '{Romance}.docx

./Iris Bromige:
Iris Bromige - Frumoasa necunoscuta 0.99 '{Romance}.docx
Iris Bromige - Lectia de sinceritate 0.99 '{Romance}.docx
Iris Bromige - Vreme rea 0.99 '{Romance}.docx

./Iris Johansen:
Iris Johansen - Lantul minciunilor 0.9 '{Romance}.docx
Iris Johansen - Printul desertului 1.0 '{Romance}.docx

./Iris Murdoch:
Iris Murdoch - Castelul de nisip 1.0 '{Dragoste}.docx
Iris Murdoch - Discipolul 0.9 '{Dragoste}.docx
Iris Murdoch - Vlastarul cuvintelor 1.0 '{Dragoste}.docx

./Iris Valentine:
Iris Valentine - Casa de langa rau 0.99 '{Romance}.docx
Iris Valentine - Lacrimi de fericire 0.99 '{Dragoste}.docx

./Irvin D. Yalom:
Irvin D. Yalom - Calatoria catre sine 1.0 '{Psihologie}.docx
Irvin D. Yalom - Calaul dragostei si alte povesti 1.0 '{Psihologie}.docx
Irvin D. Yalom - Mama si sensul vietii 1.0 '{Psihologie}.docx
Irvin D. Yalom - Minciuni pe canapea 3.0 '{Literatura}.docx
Irvin D. Yalom - Plansul lui Nietzsche 0.6 '{Psihologie}.docx
Irvin D. Yalom - Privind soarele in fata 1.0 '{Psihologie}.docx
Irvin D. Yalom - Solutia Schopenhauer 1.0 '{Psihologie}.docx

./Irving Stone:
Irving Stone - Agonie si extaz V1 1.1 '{Literatura}.docx
Irving Stone - Agonie si extaz V2 1.1 '{Literatura}.docx
Irving Stone - Bucuria vietii 1.0 '{Biografie}.docx
Irving Stone - Freud - V1 Turnul nebunilor 0.99 '{Literatura}.docx
Irving Stone - Freud - V2 Paria 1.0 '{Literatura}.docx

./Irwin Shaw:
Irwin Shaw - Om bogat, om sarac 3.0 '{Literatura}.docx

./Isaac Asimov:
Isaac Asimov - Autobiografie 1.0 '{Biografie}.docx
Isaac Asimov - Calatorie Fantastica - V1 Calatorie fantastica 1.0 '{SF}.docx
Isaac Asimov - Calatorie Fantastica - V2 Destinatia creierul 1.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Conflict evitabil 7.1 '{SF}.docx
Isaac Asimov - Eu, robotul - Evadare 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Evidenta 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Fuga in cerc 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Intai sa prindem iepurele 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Introducere 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Mincinosul 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Rationament 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - Robbie 5.0 '{SF}.docx
Isaac Asimov - Eu, robotul - S-a pierdut un robot 5.0 '{SF}.docx
Isaac Asimov - Fundatia - V1 Fundatia 3.0 '{SF}.docx
Isaac Asimov - Fundatia - V2 Fundatia si imperiul 2.0 '{SF}.docx
Isaac Asimov - Fundatia - V3 A doua Fundatie 2.0 '{SF}.docx
Isaac Asimov - Fundatia - V4 Marginea Fundatiei 2.0 '{SF}.docx
Isaac Asimov - Fundatia - V5 Fundatia si Pamantul 2.0 '{SF}.docx
Isaac Asimov - Fundatia - V6 Preludiul Fundatiei 1.0 '{SF}.docx
Isaac Asimov - Fundatia - V7 Fundatia renascuta 2.0 '{SF}.docx
Isaac Asimov - Fundatia - V8 Teama Fundatiei 4.0 '{SF}.docx
Isaac Asimov - Fundatia - V9 Fundatie si haos 4.0 '{SF}.docx
Isaac Asimov - Fundatia - V10 Triumful Fundatiei 4.0 '{SF}.docx
Isaac Asimov - Fundatia - V11 Prietenii Fundatiei 1.0 '{SF}.docx
Isaac Asimov - Galactic - V1 Pulbere de stele 3.0 '{SF}.docx
Isaac Asimov - Galactic - V2 Curentii spatiului 3.0 '{SF}.docx
Isaac Asimov - Galactic - V3 O piatra pe cer 4.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Baietelul cel urat 1.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Corbii blanzi 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - In Marsport, fara Hilda 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Numele meu incepe cu S 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Profesiunea 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Sentimentul puterii 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Sfarsitul noptii 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Toate necazurile lumii 5.0 '{SF}.docx
Isaac Asimov - Intrebare Finala - Ultima intrebare 0.9 '{SF}.docx
Isaac Asimov - Intre un pas si celelalt 0.99 '{SF}.docx
Isaac Asimov - Lucky Starr - V1 1952 Ratacitor in spatiu 2.0 '{SF}.docx
Isaac Asimov - Lucky Starr - V2 1953 Lucky Starr si piratii de pe asteroizi 1.0 '{SF}.docx
Isaac Asimov - Lucky Starr - V3 1954 Fantomele Soarelui 2.0 '{SF}.docx
Isaac Asimov - Lucky Starr - V4 1956 Lucky Starr si marele soare al lui Mercur 1.0 '{SF}.docx
Isaac Asimov - Lucky Starr - V5 1957 Robotul de pe Jupiter 2.0 '{SF}.docx
Isaac Asimov - Lucky Starr - V6 1958 Lucky Starr si inelele lui Saturn 1.0 '{SF}.docx
Isaac Asimov - Nemesis 2.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Bila de biliard 1.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Bufonul 1.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Ce-si face omul cu mana lui 2.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Clopotul muzical 1.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Condamnare la moarte 1.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Intemeietorul 2.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Omul bicentenar 2.0 '{SF}.docx
Isaac Asimov - Omul Bicentenar - Procedeul Holmes Ginscart 5.0 '{SF}.docx
Isaac Asimov - Planeta care nu a existat 0.9 '{SF}.docx
Isaac Asimov - Poezie imateriala 1.0 '{SF}.docx
Isaac Asimov - Povestiri - Calea martiana si alte povestiri V1 1.0 '{SF}.docx
Isaac Asimov - Povestiri - Eu, robotul 6.0 '{SF}.docx
Isaac Asimov - Povestiri - Intemeietorii 1.0 '{SF}.docx
Isaac Asimov - Povestiri - Intrebarea finala 1.0 '{SF}.docx
Isaac Asimov - Povestiri - Magie 1.0 '{SF}.docx
Isaac Asimov - Povestiri - Perioada Campbell 0.99 '{SF}.docx
Isaac Asimov - Povestiri - Povestiri cu roboti 2.0 '{SF}.docx
Isaac Asimov - Povestiri - Vantul schimbarii 1.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Corectorul 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Lenny 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Prima lege 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Risc 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Robotul al-76 o ia razna 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Sa ne unim 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Satisfactie garantata 5.0 '{SF}.docx
Isaac Asimov - Povestiri cu roboti - Victorie involuntara 5.0 '{SF}.docx
Isaac Asimov - Respiratia mortii 2.0 '{SF}.docx
Isaac Asimov - Roboti - V1 Cavernele de otel 6.0 '{SF}.docx
Isaac Asimov - Roboti - V2 Soarele gol 4.0 '{SF}.docx
Isaac Asimov - Roboti - V3 Robotii de pe Aurora 5.0 '{SF}.docx
Isaac Asimov - Roboti - V4 Robotii si imperiul 3.0 '{SF}.docx
Isaac Asimov - Roboti - V5 Omul pozitronic 4.0 '{SF}.docx
Isaac Asimov - Robotul disparut 0.99 '{SF}.docx
Isaac Asimov - Sfarsitul eternitatii 6.0 '{SF}.docx
Isaac Asimov - Solutia finala 1.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Absolut sigur 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Bun gust 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Cel din urma raspuns 1.1 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Convingere 5.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Cum s-a intamplat 1.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Despre nimic 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Dintr-o privire 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Ideile mor greu 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Memorie interzisa 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Moartea unui Foy 5.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Ne-au gasit 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Nimic pentru nimic 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Noaptea muzicii 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - O potrivire perfecta 5.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Pentru pasari 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Punct de aprindere 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Schimb cinstit 5.0 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Surasul pagubas 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Ultima naveta 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Vantul schimbarii 0.99 '{SF}.docx
Isaac Asimov - Vantul Schimbarii - Vine! 0.9 '{SF}.docx
Isaac Asimov - Visele sunt sacre 0.99 '{SF}.docx
Isaac Asimov - Visuri de robot 1.0 '{SF}.docx
Isaac Asimov - Zeii insisi 1.0 '{SF}.docx

./Isaac Asimov & Robert Silverberg:
Isaac Asimov & Robert Silverberg - Baietelul cel urat 1.0 '{SF}.docx
Isaac Asimov & Robert Silverberg - Caderea noptii 2.0 '{SF}.docx

./Isaac Hooke:
Isaac Hooke - Novice fara nume de cod 1.0 '{SF}.docx
Isaac Hooke - O zi obisnuita 1.0 '{SF}.docx

./Isabel Allende:
Isabel Allende - Amantul japonez 1.0 '{Literatura}.docx
Isabel Allende - Caietul Mayei 1.0 '{Literatura}.docx
Isabel Allende - Casa spiritelor 1.0 '{Politista}.docx
Isabel Allende - Despre dragoste si umbra 0.99 '{Dragoste}.docx
Isabel Allende - Eva Luna 0.9 '{Politista}.docx
Isabel Allende - Fiica norocului 1.0 '{Politista}.docx
Isabel Allende - Ines a sufletului meu 1.0 '{Politista}.docx
Isabel Allende - Insula de sub mare 1.0 '{Politista}.docx
Isabel Allende - Jocul Ripper 1.0 '{Politista}.docx
Isabel Allende - Paula 0.99 '{Politista}.docx
Isabel Allende - Planul infinit 0.99 '{Politista}.docx
Isabel Allende - Portret in sepia 1.0 '{Politista}.docx
Isabel Allende - Suma zilelor 0.9 '{Politista}.docx
Isabel Allende - Zorro 2.0 '{CapasiSpada}.docx

./Isabelle Eberhardt:
Isabelle Eberhardt - Iubiri nomade 1.0 '{Dragoste}.docx

./Isabelle Marais:
Isabelle Marais - Mai mult decat iubiti 0.9 '{Dragoste}.docx

./Isac Peltz:
Isac Peltz - Calea Vacaresti 1.0 '{Literatura}.docx
Isac Peltz - Foc in hanul cu tei 1.0 '{Dragoste}.docx
Isac Peltz - Noptile domnisoarei Mili 0.9 '{Dragoste}.docx

./Isidor Epstein:
Isidor Epstein - Iudaismul - Origini si istorie 0.99 '{Religie}.docx

./Ismail Kadare:
Ismail Kadare - Aprilie spulberat 1.0 '{Literatura}.docx
Ismail Kadare - Florile inghetate din Martie 0.9 '{Literatura}.docx
Ismail Kadare - Generalul armatei moarte 1.0 '{Literatura}.docx

./Isobel Cook:
Isobel Cook - Juramantul sub stele 0.99 '{Dragoste}.docx
Isobel Cook - Planul lui Liz 0.99 '{Dragoste}.docx

./Istvan Korda:
Istvan Korda - Mlastinile se apara 2.0 '{Western}.docx

./Isutin Petrica:
Isutin Petrica - Din memoriile lui Eugen Cristescu 0.99 '{Istorie}.docx

./Italo Calvino:
Italo Calvino - Castelul destinelor incrucisate 1.0 '{Literatura}.docx
Italo Calvino - Cosmicomicarii. Indice zero 1.0 '{Literatura}.docx
Italo Calvino - Daca intr-o noapte de iarna, un calator 1.0 '{Literatura}.docx
Italo Calvino - Orasele invizibile 1.0 '{Literatura}.docx
Italo Calvino - Palomar 2.0 '{Literatura}.docx
Italo Calvino - Povestiri 1.0 '{Literatura}.docx
Italo Calvino - Strabunii Nostri - V1 Vicontele taiat in doua 2.0 '{Literatura}.docx
Italo Calvino - Strabunii Nostri - V2 Baronul din copaci 2.0 '{Literatura}.docx
Italo Calvino - Strabunii Nostri - V3 Cavalerul inexistent 1.0 '{Literatura}.docx

./Italo Svevo:
Italo Svevo - Senilitate 1.0 '{Literatura}.docx

./Iulia Hasdeu:
Iulia Hasdeu - A fi iubita 1.0 '{Versuri}.docx

./Iulian Comanescu:
Iulian Comanescu - Dilemele lui Comanescu 0.9 '{Jurnalism}.docx

./Iulian Iamandi:
Iulian Iamandi - Trei in iad 0.99 '{Diverse}.docx

./Iulian Semionov:
Iulian Semionov - Maiorul Vifor 1.0 '{ActiuneComando}.docx
Iulian Semionov - Strada Ogariov Nr. 6 0.8 '{Politista}.docx

./Iuri Dold Mihailik:
Iuri Dold Mihailik - Singur printre dusmani V1 2.0 '{Suspans}.docx
Iuri Dold Mihailik - Singur printre dusmani V2 2.0 '{Suspans}.docx

./Iuri Klarov:
Iuri Klarov - Triunghiul de aur 1.0 '{Politista}.docx
Iuri Klarov - Triunghiul de negru 0.9 '{Politista}.docx

./Iuri Pavlovici Safronov & Svetlana Alexandrovna:
Iuri Pavlovici Safronov & Svetlana Alexandrovna - Nepotii nepotilor nostri 1.0 '{SF}.docx

./Iuri Trifonov:
Iuri Trifonov - Casa de pe chei. Alta viata 1.0 '{Dragoste}.docx

./Ivan Alexandrovici Goncearov:
Ivan Alexandrovici Goncearov - Oblomov 2.0 '{Literatura}.docx
Ivan Alexandrovici Goncearov - O poveste obisnuita 1.0 '{Literatura}.docx
Ivan Alexandrovici Goncearov - Rapa 1.0 '{Literatura}.docx

./Ivan Antonovici Efremov:
Ivan Antonovici Efremov - Atolul Facaofo 0.9 '{SF}.docx
Ivan Antonovici Efremov - Corabii astrale 0.9 '{SF}.docx
Ivan Antonovici Efremov - Coroana neagra 1.0 '{SF}.docx
Ivan Antonovici Efremov - Cor serpentis 0.99 '{SF}.docx
Ivan Antonovici Efremov - Lacul duhurilor de munte 5.0 '{SF}.docx
Ivan Antonovici Efremov - La hotarul Oicumenei 1.0 '{SF}.docx
Ivan Antonovici Efremov - Limanul curcubeului 1.0 '{SF}.docx
Ivan Antonovici Efremov - Nebuloasa din Andromeda 1.1 '{SF}.docx
Ivan Antonovici Efremov - Umbra dinosaurului 0.99 '{SF}.docx

./Ivan Arjentinski:
Ivan Arjentinski - Intamplari neverosimile 1.0 '{Literatura}.docx

./Ivan Baltor:
Ivan Baltor - Memoriile unui ofiter de contrainformatii militare 1.0 '{Biografie}.docx

./Ivan Lungu:
Ivan Lungu - File de amintire 2.2 '{Diverse}.docx

./Ivan Turgheniev:
Ivan Turgheniev - Destelenire 1.0 '{Literatura}.docx
Ivan Turgheniev - Fum 2.0 '{Literatura}.docx
Ivan Turgheniev - In ajun 1.0 '{Literatura}.docx
Ivan Turgheniev - Parinti si copii 1.0 '{Literatura}.docx
Ivan Turgheniev - Povestirile unui vanator 1.0 '{Literatura}.docx
Ivan Turgheniev - Prima iubire. Fum 1.0 '{Literatura}.docx
Ivan Turgheniev - Un cuib de nobili 0.8 '{Literatura}.docx

./Iv Martinovici:
Iv Martinovici - Am lasat graurii sa zboare 1.0 '{Politista}.docx

./Ivy Valdes:
Ivy Valdes - Umbre din trecut 1.0 '{Romance}.docx
```

